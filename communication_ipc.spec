%define debug_package %{nil}
%global build_opt /opt/distributed-middleware-build
%global oh_version OpenHarmony-v3.2-Release
%global oe_version openEuler-22.03-LTS-SP2
%global communication_path %{_builddir}/foundation/communication
%global third_party_path %{_builddir}/third_party
%global binder_dest_path /usr/lib/modules/%(uname -r)/binder

Name:           communication_ipc
Version:        1.0.0
Release:        1%{?dist}
Summary:        Inter-process communication (IPC) and Remote Procedure Call (RPC)
License:        Apache-2.0 and GPL-2.0
URL:            https://gitee.com/openharmony/communication_ipc
Source0:        https://gitee.com/openharmony/communication_ipc/repository/archive/OpenHarmony-v3.2-Release.tar.gz #/communication_ipc-OpenHarmony-v3.2-Release.tar.gz
Source1:        https://gitee.com/openharmony/communication_dsoftbus/repository/archive/OpenHarmony-v3.2-Release.tar.gz #/communication_dsoftbus-OpenHarmony-v3.2-Release.tar.gz
Source2:        https://gitee.com/openharmony/third_party_sqlite/repository/archive/OpenHarmony-v3.2-Release.tar.gz #/third_party_sqlite-OpenHarmony-v3.2-Release.tar.gz
Source3:        https://gitee.com/openharmony/third_party_libcoap/repository/archive/OpenHarmony-v3.2-Release.tar.gz #/third_party_libcoap-OpenHarmony-v3.2-Release.tar.gz
Source4:        https://gitee.com/openharmony/third_party_mbedtls/repository/archive/OpenHarmony-v3.2-Release.tar.gz #/third_party_mbedtls-OpenHarmony-v3.2-Release.tar.gz
Source5:        https://gitee.com/openeuler/kernel/binder-%{oe_version}.tar.gz
Patch0:         0001-remove-dependence-on-access-token-and-hitrace.patch
Patch1:         0002-fix-build-error-from-header-include.patch
Patch2:         0003-adapt-binder-as-a-kernel-module.patch
Patch3:         0004-adapt-compilation-for-softbus_client.patch

BuildRequires:  gcc, make, kernel-devel, uname-build-checks, distributed-build
Requires: hilog, commonlibrary_c_utils

%description
The inter-process communication (IPC) and remote procedure call (RPC) mechanisms are used to implement cross-process communication.

# Decompress source code package, make patches to the source code.
%prep
rm -rf %{_builddir}/*

cp -rp %{build_opt} %{_builddir}/build
[ ! -L "%{_builddir}/build.sh" ] && ln -s %{_builddir}/build/build_scripts/build.sh %{_builddir}/build.sh
[ ! -L "%{_builddir}/.gn" ] && ln -s %{_builddir}/build/core/gn/dotfile.gn %{_builddir}/.gn
[ ! -L "%{_builddir}/build.py" ] && ln -s %{_builddir}/build/lite/build.py %{_builddir}/build.py
cp -r %{_builddir}/build/openeuler/vendor %{_builddir}/
cp -r %{_builddir}/build/openeuler/compiler_gn/third_party %{_builddir}/
cp -rf %{_builddir}/build/openeuler/compiler_gn/base %{_builddir}
rm -rf %{_builddir}/base/security
cp -rf %{_builddir}/build/openeuler/compiler_gn/commonlibrary %{_builddir}/
rm -rf %{_builddir}/third_party/mbedtls

%setup -q -D -T -a 0 -c -n %{communication_path}
mv %{communication_path}/%{name} %{communication_path}/ipc
%patch -P0 -p1 -d %{communication_path}/ipc
%patch -P1 -p1 -d %{communication_path}/ipc

%setup -q -D -T -a 1 -c -n %{communication_path}
mv %{communication_path}/communication_dsoftbus-%{oh_version} %{communication_path}/dsoftbus
%patch -P3 -p1 -d %{communication_path}/dsoftbus

%setup -q -D -T -a 2 -c -n %{third_party_path}
mv %{third_party_path}/third_party_sqlite-%{oh_version} %{third_party_path}/sqlite

%setup -q -D -T -a 3 -c -n %{third_party_path}
mv %{third_party_path}/third_party_libcoap-%{oh_version} %{third_party_path}/libcoap

%setup -q -D -T -a 4 -c -n %{third_party_path}
mv %{third_party_path}/third_party_mbedtls-%{oh_version} %{third_party_path}/mbedtls

%setup -q -D -T -a 5 -c -n %{_builddir}
%patch -P2 -p1 -d %{_builddir}/binder

%build
cd %{_builddir}

%ifarch x86_64
bash build.sh --product-name openeuler --target-cpu x86_64
%endif

%ifarch aarch64
bash build.sh --product-name openeuler --target-cpu arm64
%endif

# build binder
cd %{_builddir}/binder
make %{?_smp_mflags}

%install
install -d -m 0755 %{buildroot}/%{_includedir}/ipc
install -d -m 0755 %{buildroot}/%{_libdir}
install -d -m 0755 %{buildroot}/system/lib64

%ifarch aarch64
module_out_path="out/openeuler/linux_clang_arm64"
header_out_path="out/openeuler/innerkits/linux-arm64/ipc"
%endif
%ifarch x86_64
module_out_path="out/openeuler/linux_clang_x86_64"
header_out_path="out/openeuler/innerkits/linux-x86_64/ipc"
%endif

# install libs and headers from ipc
install -m 0755 %{_builddir}/${module_out_path}/communication/ipc/*.so %{buildroot}%{_libdir}
install -m 0755 %{_builddir}/${module_out_path}/communication/ipc/*.so %{buildroot}/system/lib64
find %{_builddir}/${header_out_path} -name *.h -print0 | xargs -0 -i cp -rf {} %{buildroot}%{_includedir}/ipc/

# install libs and headers from dsoftbus
dsoftbus_module_path=${module_out_path}/communication/dsoftbus
rm -f %{_builddir}/${dsoftbus_module_path}/libsoftbus_server.z.so
install -m 0755 %{_builddir}/${dsoftbus_module_path}/*.so %{buildroot}%{_libdir}
install -m 0755 %{_builddir}/${dsoftbus_module_path}/*.so %{buildroot}/system/lib64

# install libs and headers from third party components
third_party_module_path=${module_out_path}/common/common
install -m 0755 %{_builddir}/${third_party_module_path}/*.so %{buildroot}%{_libdir}
install -m 0755 %{_builddir}/${third_party_module_path}/*.so %{buildroot}/system/lib64
install -m 0755 %{_builddir}/${module_out_path}/common/dsoftbus/*.so %{buildroot}%{_libdir}
install -m 0755 %{_builddir}/${module_out_path}/common/dsoftbus/*.so %{buildroot}/system/lib64

# install ko file of binder
cd %{_builddir}/binder
install -d %{buildroot}/%{binder_dest_path}/
install -m 0755 binder_linux.ko %{buildroot}/%{binder_dest_path}/

%files
%{_libdir}/*.so
%{_includedir}/ipc/*
/system/*
/%{binder_dest_path}/*.ko

%changelog
* Sun Oct 08 2023 Peng He <hepeng68@huawei.com> - 1.0.0-1
- Init package
